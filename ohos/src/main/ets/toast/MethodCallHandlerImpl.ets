/*
 * Copyright (c) 2023 Hunan OpenValley Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import promptAction from '@ohos.promptAction'

import { MethodCallHandler, MethodResult } from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodChannel';
import MethodCall from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodCall';
import Log from '@ohos/flutter_ohos/src/main/ets/util/Log';

const TAG = 'MethodCallHandlerImpl';
export default class MethodCallHandlerImpl implements MethodCallHandler{
  onMethodCall(call: MethodCall, result: MethodResult): void {
    switch (call.method) {
      case 'showToast': {
        let msg: string = call.argument('msg');

        /* dart层传入时间单位为 s, promptAction.showToast 传入时间单位为ms */
        let time: number = call.argument('time') * 1000;
        let bgcolor: string = '#' + call.argument("bgcolor").toString(16);
        let textcolor: string = '#' + call.argument("textcolor").toString(16);
        let textSize: number = call.argument("fontSize");
        let gravity: Alignment;

        if (call.argument("gravity") === 'top') {
          gravity = Alignment.Top;
        } else if (call.argument("gravity") === 'center') {
          gravity = Alignment.Center;
        } else {
          gravity = Alignment.Bottom;
        }

        try {
          promptAction.showToast({
            message: msg,
            duration: time,
            backgroundColor: bgcolor,
            textColor: textcolor,
            alignment: gravity,
            backgroundBlurStyle: BlurStyle.NONE
          });
        } catch (e) {
          Log.e(TAG, "Show toast err " + e);
        }

        result.success(true);
      }
      case 'cancel': {
        result.success(true);
      }
      default: {
        result.notImplemented();
      }
    }
  }
}